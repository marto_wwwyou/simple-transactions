<?php

define('CURRENT_DIRECTORY', dirname(__FILE__) . '/');
$autoloadPath = CURRENT_DIRECTORY . '/../vendor/autoload.php';

try {
    if (!is_file($autoloadPath)) {
        throw new Exception('Vendor folder is missing. Please, run the composer install command.');
    }

    require_once $autoloadPath;

    $euro = (new \Sti\Currency\Currency())
        ->setCurrencyCode('EUR')
        ->setCurrencyRate(1);

    $jpy = (new \Sti\Currency\Currency())
        ->setCurrencyCode('JPY')
        ->setCurrencyRate(129.53);

    $usd = (new \Sti\Currency\Currency())
        ->setCurrencyCode('USD')
        ->setCurrencyRate(1.1497);

    // Init the default currency
    \Sti\Currency\MultiCurrencyAmount::setBaseCurrency($euro);

    $container = new \Sti\Container();
    $container->setReader(new \Sti\Readers\File\CsvFileReader());
    $container->setAmountClass(function (float $amount, \Sti\Currency\Interfaces\CurrencyInterface $currency = null) {
        return new \Sti\Currency\MultiCurrencyAmount($amount, $currency);
    });
    $container->setEntryClass(function () {
        return new \Sti\Transactions\Entries\TransactionEntry();
    });

    $commissionGenerator = new \Sti\Commission\Commission();
    $commissionGenerator->setContainer($container);

    $container->setCommissionGenerator($commissionGenerator);
    $container->setMath(new \Sti\Currency\Utils\Math());
    $container->setStorage(new \Sti\Storage\MemoryStorage());
    $container->addCurrency($euro);
    $container->addCurrency($jpy);
    $container->addCurrency($usd);

    $importCommand = new \Sti\Commands\ImportCommand();
    $importCommand->setContainer($container);

    $cli = (new \Sti\Cli())
        ->registerDefaultApplication()
        ->registerCommand($importCommand)
        ->setContainer($container);

    $cli->getApplication()
        ->run();

} catch (Throwable $e) {
    echo $e->getMessage();
}