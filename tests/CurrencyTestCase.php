<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace StiTest;

use PHPUnit\Framework\TestCase;
use Sti\Currency\Currency;
use Sti\Currency\Exceptions\InvalidCurrencyCodeException;

class CurrencyTestCase extends TestCase
{
    public function testCurrencyCodeValidator()
    {
        $this->expectException(InvalidCurrencyCodeException::class);

        $currency = (new Currency())
            ->setCurrencyCode('EURO')
            ->setCurrencyRate(1);
    }

    public function testValidCurrencyObject()
    {
        $currency = (new Currency())
            ->setCurrencyCode('EUR')
            ->setCurrencyRate(1);

        $this->assertInstanceOf(Currency::class, $currency);
    }
}
