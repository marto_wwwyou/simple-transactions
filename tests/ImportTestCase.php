<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace StiTest;

use PHPUnit\Framework\TestCase;
use Sti\Readers\File\CsvFileReader;

class ImportTestCase extends TestCase
{
    public function testFileNotFound()
    {
        $this->expectException(\Exception::class);

        $reader = new CsvFileReader();
        $reader
            ->setPath('')
            ->read();
    }

    public function testRead()
    {
        $path = dirname(__FILE__).'/files/test.csv';
        $reader = new CsvFileReader();
        $reader
            ->setPath($path)
            ->read();

        $this->assertInternalType('array', $reader->data());
    }

    public function testReadCount()
    {
        $path = dirname(__FILE__).'/files/test.csv';
        $reader = new CsvFileReader();
        $reader
            ->setPath($path)
            ->read();

        $this->assertEquals(13, count($reader->data()));
    }
}
