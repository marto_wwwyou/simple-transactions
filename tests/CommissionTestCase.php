<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace StiTest;

use Carbon\Carbon;
use PHPStan\Testing\TestCase;
use Sti\Commission\Commission;
use Sti\Container;
use Sti\Currency\Utils\Math;
use Sti\Storage\MemoryStorage;
use Sti\Transactions\Entries\TransactionEntry;

class CommissionTestCase extends TestCase
{
    public function testSimpleCommission()
    {
        $euro = (new \Sti\Currency\Currency())
            ->setCurrencyCode('EUR')
            ->setCurrencyRate(1);

        $jpy = (new \Sti\Currency\Currency())
            ->setCurrencyCode('JPY')
            ->setCurrencyRate(129.53);

        $usd = (new \Sti\Currency\Currency())
            ->setCurrencyCode('USD')
            ->setCurrencyRate(1.1497);

        // Init the default currency
        \Sti\Currency\MultiCurrencyAmount::setBaseCurrency($euro);

        $container = new Container();
        $container->setStorage(new MemoryStorage());
        $container->setMath(new Math());
        $container->setAmountClass(function (float $amount, \Sti\Currency\Interfaces\CurrencyInterface $currency = null) {
            return new \Sti\Currency\MultiCurrencyAmount($amount, $currency);
        });

        $container->addCurrency($euro);
        $container->addCurrency($jpy);
        $container->addCurrency($usd);

        $commission = new Commission();
        $commission->setContainer($container);
        $date = Carbon::createFromFormat('Y-m-d', '2018-12-14', 'Europe/Sofia');

        $transaction = (new TransactionEntry())
            ->setIdentifier(1)
            ->setAmount($container->createNewAmount(100))
            ->setCurrency($euro)
            ->setDate($date)
            ->setOperation('cash_out')
            ->setType('legal');

        $this->assertEquals($commission->generateCommission($transaction), 0.5);
    }

    public function testCommission()
    {
        $euro = (new \Sti\Currency\Currency())
            ->setCurrencyCode('EUR')
            ->setCurrencyRate(1);

        $jpy = (new \Sti\Currency\Currency())
            ->setCurrencyCode('JPY')
            ->setCurrencyRate(129.53);

        $usd = (new \Sti\Currency\Currency())
            ->setCurrencyCode('USD')
            ->setCurrencyRate(1.1497);

        // Init the default currency
        \Sti\Currency\MultiCurrencyAmount::setBaseCurrency($euro);

        $container = new Container();
        $container->setStorage(new MemoryStorage());
        $container->setMath(new Math());
        $container->setAmountClass(function (float $amount, \Sti\Currency\Interfaces\CurrencyInterface $currency = null) {
            return new \Sti\Currency\MultiCurrencyAmount($amount, $currency);
        });

        $container->addCurrency($euro);
        $container->addCurrency($jpy);
        $container->addCurrency($usd);

        $commission = new Commission();
        $commission->setContainer($container);
        $date = Carbon::createFromFormat('Y-m-d', '2018-12-14', 'Europe/Sofia');

        $transaction = (new TransactionEntry())
            ->setIdentifier(1)
            ->setAmount($container->createNewAmount(100))
            ->setCurrency($euro)
            ->setDate($date)
            ->setOperation('cash_out')
            ->setType('natural');

        $transaction = (new TransactionEntry())
            ->setIdentifier(1)
            ->setAmount($container->createNewAmount(1000))
            ->setCurrency($euro)
            ->setDate($date)
            ->setOperation('cash_out')
            ->setType('natural');

        $this->assertEquals($commission->generateCommission($transaction), 0.3);
    }
}
