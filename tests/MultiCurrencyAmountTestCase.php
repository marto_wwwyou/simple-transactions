<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace StiTest;

use PHPUnit\Framework\TestCase;
use Sti\Currency\Currency;
use Sti\Currency\Interfaces\CurrencyInterface;
use Sti\Currency\Interfaces\MultiCurrencyAmountInterface;
use Sti\Currency\MultiCurrencyAmount;
use Sti\Currency\Utils\Math;

class MultiCurrencyAmountTestCase extends TestCase
{
    public function testDefaultCurrency()
    {
        $currency = (new Currency())
            ->setCurrencyCode('EUR')
            ->setCurrencyRate(1);

        MultiCurrencyAmount::setBaseCurrency($currency);

        $this->assertInstanceOf(CurrencyInterface::class, MultiCurrencyAmount::getBaseCurrency());
    }

    public function testFactory()
    {
        $this->assertInstanceOf(
            MultiCurrencyAmountInterface::class,
            MultiCurrencyAmount::fromFloat(10)
        );
    }

    public function testDefaultCurrencyAmount()
    {
        $currencyEUR = (new Currency())
            ->setCurrencyCode('EUR')
            ->setCurrencyRate(1);

        $currencyUSD = (new Currency())
            ->setCurrencyCode('USD')
            ->setCurrencyRate(1.1497);

        MultiCurrencyAmount::setBaseCurrency($currencyEUR);

        $amount = new MultiCurrencyAmount(100, $currencyUSD);

        $this->assertEquals($amount->toFixed(3), 86.979);
    }

    public function testMathAdd()
    {
        $currencyEUR = (new Currency())
            ->setCurrencyCode('EUR')
            ->setCurrencyRate(1);

        $currencyUSD = (new Currency())
            ->setCurrencyCode('USD')
            ->setCurrencyRate(1.1497);

        MultiCurrencyAmount::setBaseCurrency($currencyEUR);
        $mathService = new Math();

        $amountEur = new MultiCurrencyAmount(100);
        $amountUSD = new MultiCurrencyAmount(100, $currencyUSD);

        $this->assertEquals($mathService->add($amountEur, $amountUSD)->toFixed(3), 186.979);
    }
}
