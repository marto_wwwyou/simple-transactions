<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Commands;

use Carbon\Carbon;
use Sti\Container;
use Sti\ContainerAware;
use Sti\Currency\Exceptions\InvalidCurrencyCodeException;
use Sti\Currency\Interfaces\CurrencyInterface;
use Sti\Readers\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class ImportCommand extends Command implements ContainerAware
{

    /**
     * Stores the reader call.
     * @var Reader
     */
    protected $reader = null;

    /**
     * Stores the container class.
     * @var Container
     */
    protected $container = null;

    /**
     * Returns the container object.
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * Sets the container object.
     */
    public function setContainer(Container $container): ImportCommand
    {
        $this->container = $container;
        return $this;
    }

    protected function configure()
    {
        $this
            ->setName('import')
            ->setDescription('Import transaction')
            ->setHelp('Import a transaction CSV file')
            ->addArgument('path', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');

        if (!file_exists($path)) {
            throw new FileNotFoundException();
        }

        $data = $this
            ->getContainer()
            ->getReader()
            ->setPath((string) $path)
            ->read()
            ->data();

        foreach ($data as $entryData) {
            $entry = $this->container->createEmptyTransaction();

            $currency = $this->container->getCurrencyByCode($entryData[5]);

            if (!$currency instanceof CurrencyInterface) {
                throw new InvalidCurrencyCodeException();
            }

            $amount = $this->container->createNewAmount($entryData[4], $currency);

            $entry->setDate(Carbon::createFromFormat('Y-m-d', $entryData[0]));
            $entry->setIdentifier($entryData[1]);
            $entry->setType($entryData[2]);
            $entry->setOperation($entryData[3]);
            $entry->setAmount($amount);
            $entry->setCurrency($currency);

            $this->container->getStorage()->store($entry);
        }

        $byUser = [];
        foreach ($this->container->getStorage()->all() as $item) {
            $result = $this->getContainer()->getCommissionGenerator()->generateCommission($item);

            echo sprintf("%.2f \n", $result);
        }
    }
}
