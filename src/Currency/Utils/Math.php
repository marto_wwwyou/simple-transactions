<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Utils;

use Sti\Currency\Interfaces\MathInterface;
use Sti\Currency\Interfaces\MultiCurrencyAmountInterface;
use Sti\Currency\MultiCurrencyAmount;

class Math implements MathInterface
{

    /**
     * Returns the sum of A and B in base currency.
     * @throws \Sti\Currency\Exceptions\InvalidCurrencyCodeException
     * @throws \Sti\Currency\Exceptions\InvalidCurrencyRateException
     */
    public function add(
        MultiCurrencyAmountInterface $a,
        MultiCurrencyAmountInterface $b
    ): MultiCurrencyAmountInterface {
        return new MultiCurrencyAmount($a->getAmount() + $b->getAmount());
    }

    /**
     * Substracts A from B.
     */
    public function subtract(
        MultiCurrencyAmountInterface $a,
        MultiCurrencyAmountInterface $b
    ): MultiCurrencyAmountInterface {
        return new MultiCurrencyAmount($a->getAmount() - $b->getAmount());
    }

    /**
     * Deviced A and B.
     */
    public function divide(MultiCurrencyAmountInterface $a, float $b): MultiCurrencyAmountInterface
    {
        return new MultiCurrencyAmount($a->getAmount() / $b);
    }

    /**
     * Multiplies a to b.
     */
    public function multiply(MultiCurrencyAmountInterface $a, float $b): MultiCurrencyAmountInterface
    {
        return new MultiCurrencyAmount($a->getAmount() * $b);
    }
}
