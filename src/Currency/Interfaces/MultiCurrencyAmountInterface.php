<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Interfaces;

use Sti\Currency\MultiCurrencyAmount;

interface MultiCurrencyAmountInterface
{
    public function __construct(float $amount = 0, CurrencyInterface $currency = null);

    /**
     * Retrurns the base interface.
     * @throws InvalidBaseCurrencyException
     */
    public static function getBaseCurrency(): CurrencyInterface;

    /**
     * Sets the base currency.
     * @void
     */
    public static function setBaseCurrency(CurrencyInterface $currency): void;

    /**
     * Returns the given currency.
     * @param  CurrencyInterface      $currency
     * @throws MissingAmountException
     */
    public function getAmount(CurrencyInterface $currency = null): float;

    /**
     * Sets the currency amount.
     * @param  CurrencyInterface            $currency
     * @throws InvalidCurrencyCodeException
     * @throws InvalidCurrencyRateException
     */
    public function setAmount(float $amount, CurrencyInterface $currency = null): MultiCurrencyAmountInterface;

    /**
     * Returns the formated amount.
     * @param CurrencyInterface $currency
     */
    public function toFixed(int $decimal, CurrencyInterface $currency = null): float;

    /**
     * Creates a new instance amount.
     * @return MultiCurrencyAmount
     */
    public static function fromFloat(float $amount, CurrencyInterface $currency = null): MultiCurrencyAmountInterface;
}
