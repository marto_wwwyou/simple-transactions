<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Interfaces;

use Sti\Currency\Exceptions\InvalidCurrencyCodeException;
use Sti\Currency\Exceptions\InvalidCurrencyRateException;
use Sti\Currency\Exceptions\InvalidCurrencySymbolPositionException;

interface CurrencyInterface
{
    /**
     * Sets the currency ISO code.
     * The code must match a /[A-Z]{3}/ pattern, otherwise the method must throw invalid exception.
     * @throws InvalidCurrencyCodeException
     */
    public function setCurrencyCode(string $code): CurrencyInterface;

    /**
     * Sets the currency exchange rate.
     * @throws InvalidCurrencyRateException
     */
    public function setCurrencyRate(float $rate): CurrencyInterface;

    /**
     * Sets the currency symbol.
     */
    public function setCurrencySymbol(string $symbol): CurrencyInterface;

    /**
     * Sets the currency symbol position.
     * @throws InvalidCurrencySymbolPositionException
     */
    public function setCurrencySymbolPosition(string $symbol): CurrencyInterface;

    /**
     * Returns the default currency position, should be either
     * before or after.
     */
    public static function getDefaultCurrencySymbolPosition(): string;

    /**
     * Returns the currency symbol position.
     */
    public function getCurrencySymbolPosition(): string;

    /**
     * Returns the currency code.
     */
    public function getCurrencyCode(): string;

    /**
     * Returns the exchange rate.
     */
    public function getCurrencyRate(): float ;
}
