<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Interfaces;

interface MathInterface
{

    /**
     * Adds two multicurrency amounts.
     */
    public function add(
        MultiCurrencyAmountInterface $a,
        MultiCurrencyAmountInterface $b
    ): MultiCurrencyAmountInterface;

    /**
     * Substracts A from B.
     */
    public function subtract(
        MultiCurrencyAmountInterface $a,
        MultiCurrencyAmountInterface $b
    ): MultiCurrencyAmountInterface;

    /**
     * Deviced A and B.
     */
    public function divide(
        MultiCurrencyAmountInterface $a,
        float $b
    ): MultiCurrencyAmountInterface;

    /**
     * Multiplies a to b.
     */
    public function multiply(
        MultiCurrencyAmountInterface $a,
        float $b
    ): MultiCurrencyAmountInterface;
}
