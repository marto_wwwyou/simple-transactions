<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Exceptions;

use Throwable;

class InvalidCurrencyCodeException extends \Exception implements \Throwable
{
    public function __construct(
        string $message = 'Invalid currency code exception. The currency code must match /[A-Z]{3}/ regex pattern.',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
