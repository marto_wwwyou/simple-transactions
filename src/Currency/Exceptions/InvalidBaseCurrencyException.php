<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Exceptions;

use Throwable;

class InvalidBaseCurrencyException extends \Exception implements \Throwable
{
    public function __construct(string $message = 'Base currency is missing.', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
