<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Exceptions;

use Throwable;

class MissingAmountException extends \Exception implements Throwable
{
    public function __construct(string $message = 'The amount array is empty.', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
