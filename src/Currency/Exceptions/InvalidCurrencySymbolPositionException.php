<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Exceptions;

use Throwable;

class InvalidCurrencySymbolPositionException extends \Exception implements \Throwable
{
    public function __construct(
        string $message = 'Invalid currency symbol position exception. Can be either before or after.',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
