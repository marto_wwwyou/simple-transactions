<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency;

use Sti\Currency\Interfaces\CurrencyInterface;
use Sti\Currency\Interfaces\MultiCurrencyAmountInterface;
use Sti\Currency\Traits\MultiCurrencyAmounTrait;

class MultiCurrencyAmount implements MultiCurrencyAmountInterface
{
    use MultiCurrencyAmounTrait;

    public function __construct(float $amount = null, CurrencyInterface $currency = null)
    {
        if ($amount !== null) {
            $this->setAmount($amount, $currency);
        }
    }
}
