<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency;

use Sti\Currency\Exceptions\InvalidCurrencyCodeException;
use Sti\Currency\Exceptions\InvalidCurrencyRateException;
use Sti\Currency\Exceptions\InvalidCurrencySymbolPositionException;
use Sti\Currency\Interfaces\CurrencyInterface;

class Currency implements CurrencyInterface
{

    /**
     * Symbol positions.
     */
    const SYMBOL_POSITION_BEFORE = 'before';
    const SYMBOL_POSITION_AFTER = 'after';

    /**
     * The currency code.
     * @var string
     */
    protected $code;

    /**
     * Stores the currency rate.
     * @var float
     */
    protected $rate = 0;

    /**
     * Stores the currency symbol.
     * @var string
     */
    protected $symbol = '';

    /**
     * @var string
     */
    protected $position = '';

    /**
     * Sets the currency ISO code.
     * The code must match a /[A-Z]{3}/ pattern, otherwise the method must throw invalid exception.
     * @throws InvalidCurrencyCodeException
     */
    public function setCurrencyCode(string $code): CurrencyInterface
    {
        if (!preg_match('/^[A-Z]{3}$/u', $code)) {
            throw new InvalidCurrencyCodeException();
        }

        $this->code = $code;
        return $this;
    }

    /**
     * Sets the currency exchange rate.
     * @throws InvalidCurrencyRateException
     */
    public function setCurrencyRate(float $rate): CurrencyInterface
    {
        if ($rate <= 0) {
            throw new InvalidCurrencyRateException();
        }

        $this->rate = $rate;
        return $this;
    }

    /**
     * Sets the currency symbol.
     */
    public function setCurrencySymbol(string $symbol): CurrencyInterface
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * Sets the currency symbol position.
     * @throws InvalidCurrencySymbolPositionException
     */
    public function setCurrencySymbolPosition(string $symbol): CurrencyInterface
    {
        if ($symbol !== static::SYMBOL_POSITION_AFTER && $symbol !== static::SYMBOL_POSITION_BEFORE) {
            throw new InvalidCurrencySymbolPositionException();
        }

        return $this;
    }

    /**
     * Returns the default currency position, should be either
     * before or after.
     * @static
     */
    public static function getDefaultCurrencySymbolPosition(): string
    {
        return static::SYMBOL_POSITION_BEFORE;
    }

    /**
     * Returns the currency symbol position.
     */
    public function getCurrencySymbolPosition(): string
    {
        return $this->position;
    }

    /**
     * Returns the currency code.
     */
    public function getCurrencyCode(): string
    {
        return $this->code;
    }

    /**
     * Returns the currency exchange rate.
     */
    public function getCurrencyRate(): float
    {
        return $this->rate;
    }
}
