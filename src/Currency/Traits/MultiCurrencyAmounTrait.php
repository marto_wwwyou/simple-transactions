<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Currency\Traits;

use Sti\Currency\Exceptions\InvalidBaseCurrencyException;
use Sti\Currency\Exceptions\InvalidCurrencyCodeException;
use Sti\Currency\Exceptions\InvalidCurrencyRateException;
use Sti\Currency\Exceptions\MissingAmountException;
use Sti\Currency\Interfaces\CurrencyInterface;
use Sti\Currency\Interfaces\MultiCurrencyAmountInterface;
use Sti\Currency\MultiCurrencyAmount;
use Sti\Helpers\NumberHelper;

trait MultiCurrencyAmounTrait
{

    /**
     * Stores the base currency.
     * @var CurrencyInterface
     */
    protected static $baseCurrency = null;

    /**
     * Stores amounts to different currencies.
     * @var array
     */
    protected $amounts = [];

    /**
     * Sets the base currency.
     * @return mixed
     */
    public static function setBaseCurrency(CurrencyInterface $currency): void
    {
        static::$baseCurrency = $currency;
    }

    /**
     * Retrurns the base interface.
     * @throws InvalidBaseCurrencyException
     */
    public static function getBaseCurrency(): CurrencyInterface
    {
        if (static::$baseCurrency === null) {
            throw new InvalidBaseCurrencyException();
        }

        return static::$baseCurrency;
    }

    /**
     * Returns the given currency.
     * @param  CurrencyInterface      $currency
     * @throws MissingAmountException
     */
    public function getAmount(CurrencyInterface $currency = null): float
    {
        if (count($this->amounts) == 0) {
            throw new MissingAmountException();
        }

        if ($currency === null) {
            $currency = static::getBaseCurrency();
        }

        // There is no amount in this currency
        if (!$this->amountInCurrencyExists($currency)) {

            // Generate it
            $this->fromBase($currency);
        }

        return $this->amounts[$currency->getCurrencyCode()];
    }

    /**
     * @throws InvalidBaseCurrencyException
     */
    protected function toBase(float $amount, CurrencyInterface $currency): void
    {
        $baseCurrency = static::getBaseCurrency();

        if (!$baseCurrency instanceof CurrencyInterface) {
            throw new InvalidBaseCurrencyException();
        }

        $this->amounts[$baseCurrency->getCurrencyCode()] = $amount / $currency->getCurrencyRate();
    }

    /**
     * Checks if the base currency exists.
     */
    protected function baseExists(): bool
    {
        return (isset($this->amounts[static::getBaseCurrency()->getCurrencyCode()]));
    }

    /**
     * Checks if the amount in the currency exists.
     */
    protected function amountInCurrencyExists(CurrencyInterface $currency): bool
    {
        return isset($this->amounts[$currency->getCurrencyCode()]);
    }

    /**
     * Generates the amount from the base currency.
     * @throws MissingAmountException
     */
    protected function fromBase(CurrencyInterface $currency)
    {
        $this->amounts[$currency->getCurrencyCode()] = $this->getAmount(static::getBaseCurrency()) * $currency->getCurrencyRate();
    }

    /**
     * Sets the currency amount.
     * @param  CurrencyInterface            $currency
     * @throws InvalidCurrencyCodeException
     * @throws InvalidCurrencyRateException
     */
    public function setAmount(float $amount, CurrencyInterface $currency = null): MultiCurrencyAmountInterface
    {
        // Clear them, so that we dont have a problem with calculations
        $this->amounts = [];

        //$amount = NumberHelper::roundFloat($amount, 2);

        if ($currency === null) {
            $currency = static::getBaseCurrency();
        }

        if ($currency->getCurrencyCode() === '') {
            throw new InvalidCurrencyCodeException();
        }

        if ($currency->getCurrencyRate() <= 0) {
            throw new InvalidCurrencyRateException();
        }

        $this->amounts[$currency->getCurrencyCode()] = $amount;

        if ($currency->getCurrencyCode() !== static::getBaseCurrency()->getCurrencyCode()) {
            $this->toBase($amount, $currency);
        }

        return $this;
    }

    /**
     * Returns the formated amount.
     * @param  CurrencyInterface      $currency
     * @throws MissingAmountException
     */
    public function toFixed(int $decimal, CurrencyInterface $currency = null): float
    {
        return NumberHelper::roundFloat($this->getAmount($currency), $decimal);
    }

    /**
     * Creates a new instance amount.
     */
    public static function fromFloat(float $amount, CurrencyInterface $currency = null): MultiCurrencyAmountInterface
    {
        return new MultiCurrencyAmount($amount, $currency);
    }
}
