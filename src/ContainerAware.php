<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti;

interface ContainerAware
{

    /**
     * Sets the container instance.
     */
    public function setContainer(Container $container);

    /**
     * Returns the container instance.
     */
    public function getContainer(): Container;
}
