<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Transactions\Entries;

use Carbon\Carbon;
use Sti\Currency\Interfaces\CurrencyInterface;
use Sti\Currency\Interfaces\MultiCurrencyAmountInterface;
use Sti\Transactions\Interfaces\TransactionEntryInterface;

class TransactionEntry implements TransactionEntryInterface
{

    /**
     * Stores the date.
     * @var null | Carbon
     */
    protected $date = null;

    /**
     * The user id.
     * @var int
     */
    protected $id = 0;

    /**
     * Sets the user type.
     * @var string
     */
    protected $type = '';

    /**
     * Sets the operation.
     * @var string
     */
    protected $operation = '';

    /**
     * Sets the user amount.
     * @var MultiCurrencyAmountInterface
     */
    protected $amount = null;

    /**
     * Stores the currency object.
     * @var CurrencyInterface
     */
    protected $currency = null;

    
    public function setDate(Carbon $date): TransactionEntryInterface
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Sets the user identification number.
     */
    public function setIdentifier(int $identification): TransactionEntryInterface
    {
        $this->id = $identification;
        return $this;
    }

    /**
     * Sets the transaction type.
     */
    public function setType(string $type): TransactionEntryInterface
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Sets the operation type.
     */
    public function setOperation(string $type): TransactionEntryInterface
    {
        $this->operation = $type;
        return $this;
    }

    /**
     * Sets the transaction amount.
     */
    public function setAmount(MultiCurrencyAmountInterface $amount): TransactionEntryInterface
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Sets the currency object.
     */
    public function setCurrency(CurrencyInterface $currency): TransactionEntryInterface
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Returns the currency.
     */
    public function getCurrency(): CurrencyInterface
    {
        return $this->currency;
    }

    /**
     * Returns the currency amount.
     */
    public function getAmount(): MultiCurrencyAmountInterface
    {
        return $this->amount;
    }

    /**
     * Returns the identification number.
     */
    public function getIdentifier(): int
    {
        return $this->id;
    }

    /**
     * Returns the transaction type.
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Returns the operation.
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * Returns the transaction date.
     */
    public function getDate(): ?Carbon
    {
        return $this->date;
    }
}
