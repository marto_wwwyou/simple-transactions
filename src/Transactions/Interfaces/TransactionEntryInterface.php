<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Transactions\Interfaces;

use Carbon\Carbon;
use Sti\Currency\Interfaces\CurrencyInterface;
use Sti\Currency\Interfaces\MultiCurrencyAmountInterface;

interface TransactionEntryInterface
{
    /**
     * Sets the transaction date.
     */
    public function setDate(Carbon $date): TransactionEntryInterface;

    /**
     * Returns the transaction date.
     */
    public function getDate(): ?Carbon;

    /**
     * Set the identification number.
     */
    public function setIdentifier(int $identifier): TransactionEntryInterface;

    /**
     * Sets the transaction type.
     */
    public function setType(string $type): TransactionEntryInterface;

    /**
     * Sets the operation type.
     */
    public function setOperation(string $type): TransactionEntryInterface;

    /**
     * Sets the transaction amount.
     * It is a good idea to convert it here to the multicurrency amount object.
     *
     */
    public function setAmount(MultiCurrencyAmountInterface $amount): TransactionEntryInterface;

    /**
     * Sets the currency object.
     */
    public function setCurrency(CurrencyInterface $currency): TransactionEntryInterface;

    /**
     * Returns the currency.
     */
    public function getCurrency(): CurrencyInterface;

    /**
     * Returns the currency amount.
     */
    public function getAmount(): MultiCurrencyAmountInterface;

    /**
     * Returns the identification number.
     */
    public function getIdentifier(): int;

    /**
     * Returns the transaction type.
     */
    public function getType(): string;

    /**
     * Returns the operation.
     */
    public function getOperation(): string;
}
