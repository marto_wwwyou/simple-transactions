<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti;

use Sti\Commands\ImportCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;

class Cli
{
    const VERSION = '1.0';

    /**
     * Stores the container instance.
     * @var Container
     */
    protected $container = null;

    /**
     * Stores the application instance.
     * @var Application
     */
    protected $_application = null;

    public function __construct()
    {
    }

    /**
     * Registeres the default application.
     */
    public function registerDefaultApplication(): Cli
    {
        $this->_application = new Application('simple-transaction', static::VERSION);
        return $this;
    }

    /**
     * Returns the application instance.
     */
    public function getApplication(): Application
    {
        return $this->_application;
    }

    /**
     * Registeres a cli command.
     */
    public function registerCommand(Command $command): Cli
    {
        $this->_application->add($command);
        return $this;
    }

    /**
     * Register the default cli commands.
     * @return $this
     */
    public function registerDefaultCommands()
    {
        $this->registerCommand(new ImportCommand());
        return $this;
    }

    /**
     * Returns the Container instance.
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * Sets the container instance.
     */
    public function setContainer(Container $container): Cli
    {
        $this->container = $container;
        return $this;
    }

    /**
     * Sets the Application instance.
     */
    public function setApplication(Application $application): Cli
    {
        $this->_application = $application;
        return $this;
    }
}
