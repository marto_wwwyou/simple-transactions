<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Readers\File;

use Sti\Readers\Reader;

class CsvFileReader extends Reader
{

    /**
     * Reads the data.
     */
    public function read(): Reader
    {
        if (file_exists($this->getPath()) && ($handle = fopen($this->getPath(), 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if (!isset($data[0]) || !preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}+$/', $data[0])) {
                    throw new \Exception('The first field must contain a valid php date in the format Y-m-d');
                }

                if (!isset($data[1]) || !is_numeric($data[1])) {
                    throw new \Exception('The user id cannot be empty and must be a valid number.');
                }

                if (!isset($data[2])
                    || !is_string($data[2])
                    || !in_array(strtolower($data[2]), ['natural', 'legal'], true)) {
                    throw new \Exception('User type must be either natural or legal.');
                }

                if (!isset($data[3])
                    || !is_string($data[3])
                    || !in_array(strtolower($data[3]), ['cash_out', 'cash_in'], true)) {
                    throw new \Exception('Cache in operation must be either cash_out or cash_in.');
                }

                if (!isset($data[4]) || !is_numeric($data[4])) {
                    throw new \Exception('The amount must be numeric.');
                }

                if (!isset($data[5])
                    || !is_string($data[5])
                    || !in_array(strtoupper($data[5]), ['EUR', 'JPY', 'USD'], true)) {
                    throw new \Exception('The supported currencies are EUR,JPY,USD');
                }

                $this->data[] = $data;
            }
            fclose($handle);
        } else {
            throw new \Exception('File cannot be opened or is missing.');
        }

        return $this;
    }

    /**
     * Return the data as an array.
     */
    public function data(): array
    {
        return $this->data;
    }
}
