<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Readers;

abstract class Reader
{
    /**
     * Stores the file path.
     * @var string
     */
    protected $path = '';

    /**
     * Stores the read data.
     * @var array
     */
    protected $data = [];

    /**
     * Returns the file path.
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Sets the path and returns the instance
     * for chainable method calling.
     */
    public function setPath(string $path): Reader
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Reads the data.
     */
    abstract public function read(): Reader;

    /**
     * Return the data as an array.
     */
    abstract public function data(): array;
}
