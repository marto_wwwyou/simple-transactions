<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Commission;

use Carbon\Carbon;
use Sti\Container;
use Sti\ContainerAware;
use Sti\Transactions\Interfaces\TransactionEntryInterface;

class Commission implements ContainerAware
{
    /**
     * Stores the container instance.
     * @var Container
     */
    protected $container = null;

    protected static $passed = [];

    /**
     * Sets the container instance.
     */
    public function setContainer(Container $container): Commission
    {
        $this->container = $container;
        return $this;
    }

    /**
     * Returns the container instance.
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    public function generateCommission(TransactionEntryInterface $transactionEntry): float
    {
        $week = ($transactionEntry->getDate() instanceof Carbon
            && $transactionEntry->getDate()->week !== null) ? $transactionEntry->getDate()->week : 0;

        if ($week === 0) {
            throw new \Exception('Cannot fetch week number');
        }

        if ($transactionEntry->getOperation() === 'cash_in') {
            $fee = $this->container
                ->getMath()
                ->multiply($transactionEntry->getAmount(), 0.0003);

            if ($fee->getAmount() > 5) {
                $result = $this->getContainer()
                    ->createNewAmount(5.00)
                    ->toFixed(2, $transactionEntry->getCurrency());
            } else {
                $result = $fee->toFixed(2, $transactionEntry->getCurrency());
            }
        } else {
            if ($transactionEntry->getType() === 'legal') {
                $fee = $this->container
                    ->getMath()
                    ->multiply($transactionEntry->getAmount(), 0.003);

                if ($fee->getAmount() < 0.5) {
                    $result = $this->getContainer()
                        ->createNewAmount(0.50)
                        ->toFixed(2, $transactionEntry->getCurrency());
                } else {
                    $result = $fee->getAmount($transactionEntry->getCurrency());
                }
            } else {

                // Check for passed transactions
                $key = $transactionEntry->getIdentifier().'-'.$transactionEntry->getOperation();

                $passed = (isset(static::$passed[$key][$week]))
                    ? static::$passed[$key][$week] : [];

                if (count($passed) > 3 || array_sum($passed) >= 1000) {
                    $result = $this->container
                        ->getMath()
                        ->multiply(
                            $transactionEntry->getAmount(),
                            0.003
                        )
                        ->toFixed(2, $transactionEntry->getCurrency());
                } else {
                    $total = (count($passed) > 0) ? array_sum($passed) : 0;

                    $left = 1000 - $total;
                    if ($left > $transactionEntry->getAmount()->getAmount()) {
                        $result = 0;
                    } else {
                        $amount = $this->getContainer()->getMath()->subtract(
                            $transactionEntry->getAmount(),
                            $this->getContainer()->createNewAmount($left)
                        );

                        $result = $this->container
                            ->getMath()
                            ->multiply(
                                $amount,
                                0.003
                            )->toFixed(2, $transactionEntry->getCurrency());
                    }
                }
            }
        }

        static::$passed
        [$transactionEntry->getIdentifier().'-'.$transactionEntry->getOperation()]
        [$week]
        [] = $transactionEntry
            ->getAmount()
            ->getAmount();

        return $result;
    }
}
