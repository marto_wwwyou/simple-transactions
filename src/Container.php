<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti;

use Sti\Commission\Commission;
use Sti\Currency\Interfaces\CurrencyInterface;
use Sti\Currency\Interfaces\MathInterface;
use Sti\Currency\Interfaces\MultiCurrencyAmountInterface;
use Sti\Readers\Reader;
use Sti\Storage\Interfaces\StorageInterface;
use Sti\Transactions\Interfaces\TransactionEntryInterface;

/**
 * Class Container.
 * @package Sti
 */
class Container
{

    /**
     * Stores the reader instance.
     * @var Reader
     */
    protected $reader = null;

    /**
     * Stores the currencies.
     * @var CurrencyInterface[]
     */
    protected $currencies = [];

    /**
     * Sets the default currency.
     * @var CurrencyInterface
     */
    protected $defaultCurrency = null;

    /**
     * Stores the multicurrency amount class.
     * @var \Closure
     */
    protected $amountClass;

    /**
     * Stores the entry class.
     * @var \Closure
     */
    protected $entryClass;

    /**
     * Stores the math operations class.
     * @var MathInterface
     */
    protected $math = null;

    /**
     * Stores the storage object.
     * @var StorageInterface
     */
    protected $storage = null;

    /**
     * Stores the comission generator.
     * @var Commission
     */
    protected $commissionGenerator = null;

    /**
     * Returns the reader.
     */
    public function getReader(): Reader
    {
        return $this->reader;
    }

    
    public function setReader(Reader $reader): Container
    {
        $this->reader = $reader;
        return $this;
    }

    /**
     * Returns the math operator.
     */
    public function getMath(): MathInterface
    {
        return $this->math;
    }

    
    public function setMath(MathInterface $math): Container
    {
        $this->math = $math;
        return $this;
    }

    
    public function setAmountClass(\Closure $amountClass): Container
    {
        $this->amountClass = $amountClass;
        return $this;
    }

    
    public function setEntryClass(\Closure $entryClass): Container
    {
        $this->entryClass = $entryClass;
        return $this;
    }

    /**
     * Sets the storage class.
     */
    public function setStorage(StorageInterface $storage): Container
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * Returns the storage object.
     */
    public function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    /**
     * Creates a entry object.
     */
    public function createEmptyTransaction(): TransactionEntryInterface
    {
        return call_user_func($this->entryClass);
    }

    /**
     * Adds a new currency.
     * @return $this
     */
    public function addCurrency(CurrencyInterface $currency)
    {
        $this->currencies[] = $currency;

        return $this;
    }

    
    public function getDefaultCurrency(): CurrencyInterface
    {
        return $this->defaultCurrency;
    }

    /**
     * @param CurrencyInterface $currency
     */
    public function createNewAmount(float $amount, CurrencyInterface $currency = null): MultiCurrencyAmountInterface
    {
        return call_user_func($this->amountClass, $amount, $currency);
    }

    /**
     * Returns the currency by code.
     * @return bool|CurrencyInterface
     */
    public function getCurrencyByCode(string $code)
    {
        foreach ($this->currencies as $currency) {
            if ($currency->getCurrencyCode() == $code) {
                return $currency;
            }
        }
        return false;
    }

    /**
     * Returns the commission generator.
     */
    public function getCommissionGenerator(): Commission
    {
        return $this->commissionGenerator;
    }

    /**
     * Sets the commission generator class.
     */
    public function setCommissionGenerator(Commission $commissionGenerator): Container
    {
        $this->commissionGenerator = $commissionGenerator;
        return $this;
    }
}
