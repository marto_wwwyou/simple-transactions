<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Helpers;

class NumberHelper
{
    public static function roundFloat($float, $neededPrecision, $startAt = 7)
    {
        if ($neededPrecision < $startAt) {
            $startAt--;
            $newFloat = round($float, $startAt);
            return static::roundFloat($newFloat, $neededPrecision, $startAt);
        }

        return $float;
    }
}
