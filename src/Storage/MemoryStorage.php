<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Storage;

use Sti\Storage\Interfaces\StorageInterface;
use Sti\Transactions\Interfaces\TransactionEntryInterface;

class MemoryStorage implements StorageInterface
{
    /**
     * Stores the transactions.
     * @var TransactionEntryInterface[]
     */
    protected $store = [];

    /**
     * Adds and entry to the storage.
     */
    public function store(TransactionEntryInterface $entry): int
    {
        $next = $this->count() + 1;
        $this->store[] = $entry;

        return $next;
    }

    /**
     * Return the storage interface.
     */
    public function all(): array
    {
        return $this->store;
    }

    /**
     * Returns an entry by key or false if not found.
     * @return bool|false|TransactionEntryInterface
     */
    public function findByIndex(int $key)
    {
        return (isset($this->store[$key])) ? $this->store[$key] : false;
    }

    /**
     * Return the total entries.
     */
    public function count(): int
    {
        return count($this->store);
    }

    /**
     * Returns the transactions by given type.
     * @return TransactionEntryInterface[] | array
     */
    public function findByType(string $type): array
    {
        return array_filter($this->store, function (TransactionEntryInterface $transactionEntry) use ($type) {
            return $type == $transactionEntry->getType();
        });
    }
}
