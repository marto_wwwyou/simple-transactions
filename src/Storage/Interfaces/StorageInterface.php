<?php

/**
 * @author  Martin Andreev <martin.andreev92@gmail.com>
 * @license MIT
 * @version 1.0.0
 */

namespace Sti\Storage\Interfaces;

use Sti\Transactions\Interfaces\TransactionEntryInterface;

interface StorageInterface
{

    /**
     * Performs a storing operation.
     */
    public function store(TransactionEntryInterface $entry): int;

    /**
     * Returns the storage.
     * @return TransactionEntryInterface[]
     */
    public function all(): array;

    /**
     * Returns the transaction by given key.
     * @return TransactionEntryInterface | false
     */
    public function findByIndex(int $key);

    /**
     * Returns the transactions by given type.
     * @return TransactionEntryInterface[] | array
     */
    public function findByType(string $type): array;

    /**
     * Returns the total count.
     */
    public function count(): int;
}
